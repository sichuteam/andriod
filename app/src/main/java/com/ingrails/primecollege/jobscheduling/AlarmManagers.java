package com.ingrails.primecollege.jobscheduling;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.ingrails.primecollege.R;


public class AlarmManagers extends AppCompatActivity {
    private static final int REQUEST_CODE = 1000;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_manager);
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setAlertMessage(3);
            }
        }, 2000);
    }

    private void setAlertMessage(int startTime) {
        Intent intent = new Intent(this, AlarmBroadcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.
                getBroadcast(this.getApplicationContext(), REQUEST_CODE, intent, 0);
        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP,
                System.currentTimeMillis() + (startTime * 1000), pendingIntent);
        Toast.makeText(this, "Alarm set in " + startTime + " seconds", Toast.LENGTH_LONG).show();
    }
}
