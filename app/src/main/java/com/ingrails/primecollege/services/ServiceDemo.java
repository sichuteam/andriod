package com.ingrails.primecollege.services;

import android.app.Service;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.io.IOException;

/**
 * Created by gokarna on 10/30/17.
 * Audio Player Service
 */

public class ServiceDemo extends Service implements MediaPlayer.OnCompletionListener {
    private AssetFileDescriptor assetFileDescriptor;

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            //asset file descriptor
            assetFileDescriptor = getApplicationContext().getAssets().openFd("funnysong.mp3");
        } catch (IOException e) {
            Toast.makeText(this, "Invalid path", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setDataSource(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return START_STICKY;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        mediaPlayer.release();
        stopSelf();
    }
}
