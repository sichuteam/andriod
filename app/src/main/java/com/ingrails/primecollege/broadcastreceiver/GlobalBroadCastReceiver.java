package com.ingrails.primecollege.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;

/**
 * Created by gokarna on 10/27/17.
 * global broad cast receiver example
 */

public class GlobalBroadCastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action != null && action.equals("android.intent.action.PHONE_STATE")) {
            Bundle extras = intent.getExtras();
            String state = extras!=null?extras.getString(TelephonyManager.EXTRA_STATE):null;
            Log.e("State", state);
            if (state != null && state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                String phoneNumber = extras
                        .getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
                Log.e("Phone Number", phoneNumber);
            }
        }
    }
}
