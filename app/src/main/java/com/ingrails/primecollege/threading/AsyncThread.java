package com.ingrails.primecollege.threading;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.ingrails.primecollege.R;

public class AsyncThread extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async);
        AsyncTask asyncTask=new WorkerTask();
        asyncTask.execute();
    }

    private class WorkerTask<Void> extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.e("Pre Execute","Pre Execute Fired in "+Thread.currentThread().getName());
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Log.e("Background","Pre Execute Fired in "+Thread.currentThread().getName());
            for(int i=0;i<10;i++){
                Log.e("Counting", String.valueOf(i));
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.e("Post Execute","Pre Execute Fired in "+Thread.currentThread().getName());
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }
}
