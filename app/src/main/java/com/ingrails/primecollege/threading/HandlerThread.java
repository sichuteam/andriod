package com.ingrails.primecollege.threading;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.widget.Toast;

import com.ingrails.primecollege.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class HandlerThread extends AppCompatActivity {
    private static android.os.Handler handler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handler_thread);
        handler=new Handler(new IncomingHandler());
        startFirstThread();
        startSecondThread();
    }

    private void startFirstThread() {
        Runnable runnable = new Runnable() {
            public void run() {
                Message message = new Message();
                Bundle bundle = new Bundle();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss", Locale.US);
                String dateString = simpleDateFormat.format(new Date());
                bundle.putString("time", dateString);
                message.setData(bundle);
                handler.sendMessage(message);
            }
        };
        handler.post(runnable);
    }

    private void startSecondThread() {
        Runnable runnable = new Runnable() {
            public void run() {
                Message message = new Message();
                Bundle bundle = new Bundle();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
                String dateString = simpleDateFormat.format(new Date());
                bundle.putString("date", dateString);
                message.setData(bundle);
                handler.sendMessage(message);
            }
        };
        handler.post(runnable);
    }

    private class IncomingHandler implements android.os.Handler.Callback {
        @Override
        public boolean handleMessage(Message message) {
            Bundle bundle = message.getData();
            if (bundle.containsKey("date")) {
                String date = bundle.getString("date");
                Toast.makeText(HandlerThread.this, date, Toast.LENGTH_SHORT).show();
                return true;
            }
            String time = bundle.getString("time");
            Toast toast = Toast.makeText(HandlerThread.this, time, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP, 0, 0);
            toast.show();
            return true;
        }
    }
}


